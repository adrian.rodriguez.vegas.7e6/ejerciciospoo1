class Estudiants (
    val nom: String = "Adrian",
    val nota: String = "Aprobat"
) {

    fun mostrarDades(){

        val estudiant1 = Estudiants()
        estudiant1.nom
        estudiant1.nota

        val estudiant2 = Estudiants()
        estudiant2.nom
        estudiant2.nota

        println("""
        ESTUDIANT 1:
        Nom: ${estudiant1.nom} , Nota: ${estudiant1.nota}
        
        ESTUDIANT 2:
        Nom: ${estudiant2.nom} , Nota: ${estudiant2.nota}
        """.trimIndent())
    }
}