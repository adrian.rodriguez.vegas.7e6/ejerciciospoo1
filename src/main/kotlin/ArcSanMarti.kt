class ArcSanMArti {
    val colors = arrayOf("vermell", "taronja", "groc", "verd", "blau", "indic", "violeta")

    fun verificarColor(color: String){
        if (color in colors){
            println("Aquest color pertany a l'arc de Sant Martí!")
        }else {
            println("No pertany a l'arc de Sant Martí!")
        }
    }
}