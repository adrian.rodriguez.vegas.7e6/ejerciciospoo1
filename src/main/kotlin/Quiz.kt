
class Quiz(
    val pregunta: String = "",
    var resposta: String = ""
) {
    fun mostrarPreguntes(pregunta: String){
        println(pregunta)
    }
    fun resposta(){
        print("Resposta: ")
        resposta = readLine()!!.toString()
    }
    fun verificarResposta(respostaCheck: String){
        if (respostaCheck == resposta){
            println("Correcte!")
        }else {
            println("Incorrecte!")
        }
    }
}


