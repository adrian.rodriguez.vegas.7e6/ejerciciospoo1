fun main() {
    //EJERCICIO1
    val estudiant1 = Estudiants("Adrian", "Bé")
    val estudiant2 = Estudiants("Gerard", "Notable")

    println("""
        NOM: ${estudiant1.nom}, NOTA: ${estudiant1.nota}
        NOM: ${estudiant2.nom}, NOTA: ${estudiant2.nota}
    """.trimIndent())

    //EJERCICIO2
    val sanMarti = ArcSanMArti()
    sanMarti.colors
    print("Introduiex un color: ")
    val color = readLine()!!.toString()
    val pertany = sanMarti.verificarColor(color)
    pertany

    //EJERCICIO3
    abstract class Instrument {
        abstract fun makeSound(): String

        fun makeSounds(temps: Int) {
            for (i in 1..temps) {
                println(makeSound())
            }
        }
    }

    class Tambor(val to: String) : Instrument() {
        override fun makeSound(): String {
            return when (to) {
                "A" -> "TAAAM"
                "O" -> "TOOOM"
                "U" -> "TUUM"
                else -> "TO DE TAMBOR DESCONEGUT"
            }
        }
    }

    class Triangle(val resonancia: Int) : Instrument() {
        override fun makeSound(): String {
            return "T" + "I".repeat(resonancia) + "NC"
        }
    }

    fun play(instruments: List<Instrument>) {
        for (instrument in instruments) {
            instrument.makeSounds(2) // plays 2 times the sound
        }
    }

    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Tambor("A"),
        Tambor("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)

    //EJERCICIO4
    val preguntaTextLliure = Quiz()
    preguntaTextLliure.mostrarPreguntes("Quin equip és el millor del món?")
    preguntaTextLliure.resposta()
    preguntaTextLliure.verificarResposta("Real Madrid")

    val preguntaMultipleChoice = Quiz()
    preguntaMultipleChoice.mostrarPreguntes("""
    Qui va guanyar la carrera de F1 ahir:
    a.Verstappen
    b.Alonso
    c.Mazepin
    """.trimIndent())
    preguntaMultipleChoice.resposta()
    preguntaMultipleChoice.verificarResposta("Alonso")

}